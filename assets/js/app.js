var mFoundation = angular.module('mFoundation', ['ui.state', 'ui.bootstrap', 'ngSanitize'])

mFoundation.value('$anchorScroll', angular.noop)
.config(function($stateProvider, $urlRouterProvider){

	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('/', {
			url: '/',
			templateUrl: 'templates/index.html',
			controller: 'IndexController'
		})
		.state('about', {
			url: '/about',
			templateUrl: 'templates/about.html',
			controller: 'AboutController'
		})
        .state('about.home', {
			url: '/',
			templateUrl: 'templates/about/tmf.html',
			controller: 'AboutController'
		})
        .state('about.tmf', {
			url: '/tmf',
			templateUrl: 'templates/about/tmf.html',
			controller: 'AboutController'
		})
        .state('about.mission', {
			url: '/mission',
			templateUrl: 'templates/about/mission.html',
			controller: 'AboutController'
		})
        .state('about.maumoon', {
			url: '/maumoon',
			templateUrl: 'templates/about/maumoon.html',
			controller: 'AboutController'
		})
        .state('about.constitution', {
			url: '/constitution',
			templateUrl: 'templates/about/constitution.html',
			controller: 'AboutController'
		}) 
        .state('about.agm', {
			url: '/annual-general-meetings',
			templateUrl: 'templates/about/annual-general-meeting.html',
			controller: 'AboutController'
		}) 
        .state('about.agm.gallery1', {
			url: '/gallery-1',
			templateUrl: 'templates/about/agm/gallery-1.html',
			controller: 'AboutController'
		}) 
        .state('about.schols', {
			url: '/scholarships',
			templateUrl: 'templates/about/scholarships.html',
			controller: 'AboutController'
		})
        .state('about.ihiyaa-programs', {
			url: '/ihiyaa-programs',
			templateUrl: 'templates/about/ihiyaa-programs.html',
			controller: 'AboutController'
		})
        .state('about.visits', {
			url: '/visits',
			templateUrl: 'templates/about/visits.html',
			controller: 'AboutController'
		})
        .state('about.publications', {
			url: '/publications',
			templateUrl: 'templates/about/publications.html',
			controller: 'AboutController'
		})
        .state('about.publications.book1', {
			url: '/book-1',
			templateUrl: 'templates/about/publications/book-1.html',
			controller: 'AboutController'
		})
        .state('areas', {
			url: '/areas',
			templateUrl: 'templates/areas.html',
			controller: 'AboutController'
		})
        .state('areas.home', {
			url: '/',
			templateUrl: 'templates/areas/islam.html',
			controller: 'AboutController'
		})
        .state('areas.islam', {
			url: '/islam',
			templateUrl: 'templates/areas/islam.html',
			controller: 'AboutController'
		})
        .state('areas.education', {
			url: '/education',
			templateUrl: 'templates/areas/education.html',
			controller: 'AboutController'
		})
        .state('areas.social', {
			url: '/social',
			templateUrl: 'templates/areas/social.html',
			controller: 'AboutController'
		})
        .state('areas.environment', {
			url: '/environment',
			templateUrl: 'templates/areas/environment.html',
			controller: 'AboutController'
		})
        .state('office', {
			url: '/office',
			templateUrl: 'templates/office/home.html',
			controller: 'AboutController'
		})
        .state('office.officeBearers', {
			url: '/office-bearers',
			templateUrl: 'templates/office/office-bearers.html',
			controller: 'AboutController'
		})
        .state('office.executiveCommittee', {
			url: '/executive-committee',
			templateUrl: 'templates/office/executive-committee.html',
			controller: 'ExecutiveController'
		})
        .state('office.boardOfTrustees', {
			url: '/board-of-trustees',
			templateUrl: 'templates/office/board-of-trustees.html',
			controller: 'BoardOfTrusteesController'
		})
		.state('history', {
			url: '/history',
			templateUrl: 'templates/history.html',
			controller: 'HistoryController'
		})
		.state('history.home', {
			url: '/',
			templateUrl: 'templates/years/1978.html',
			controller: 'HistoryController'
		})
		.state('history.1978', {
			url: '/1978',
			templateUrl: 'templates/years/1978.html',
			controller: 'HistoryController'
		})
		.state('activities', {
			url: '/activities',
			templateUrl: 'templates/activities.html',
			controller: 'ActivitiesController'
		})
		.state('gallery', {
			url: '/gallery',
			templateUrl: 'templates/gallery.html',
			controller: 'GalleryController'
		})
		.state('gallery-detail', {
			url: '/gallery-detail',
			templateUrl: 'templates/gallery-detail.html',
			controller: 'GalleryController'
		})
		.state('press', {
			url: '/press',
			templateUrl: 'templates/press.html',
			controller: 'GalleryController'
		})
		.state('privacy', {
			url: '/privacy',
			templateUrl: 'templates/privacy.html',
			controller: 'GalleryController'
		})
		.state('contact', {
			url: '/contact',
			templateUrl: 'templates/contact.html',
			controller: 'GalleryController'
		})

})
mFoundation.controller('IndexController', function($scope, $timeout, $stateParams) {
    
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
    
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
    
    jQuery(".lazy").lazy();
    
    jQuery("carousel").carousel();

});

mFoundation.controller('ExecutiveController', function($scope, $timeout, $stateParams, $location, $http) {
    
    $http.get('/application/public/static-page/executive-committee/get').success(function(resp){
        $scope.members = resp;
    });

});

mFoundation.controller('BoardOfTrusteesController', function($scope, $timeout, $stateParams, $location, $http) {
    
    $http.get('/application/public/static-page/board-of-trustees/get').success(function(resp){
        $scope.members = resp;
    });

});
mFoundation.controller('ExploreController', function($scope, $timeout, $stateParams, $location) {


});

mFoundation.controller('AboutController', function($scope, $timeout, $stateParams, $location) {
    $('.side-list a').click(function() {
        $(this).addClass('active');
        $(".side-list a").not(this).removeClass('active');
    });
});

mFoundation.controller('HistoryController', function($scope, $timeout, $stateParams, $location) {
	
	$('#history-slider').slider();
	
	$('#history-slider').on('slide', function (ev) {

        var slideYear = $('#history-slider').val();
        $scope.$apply( $location.path( "/history/" + slideYear ) );

    });
});

